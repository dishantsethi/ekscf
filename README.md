# EKS CloudFormation Template

## The EKS IAM Role

```We are required to create a new IAM role that will allow the EKS service to assume role to your AWS account.```

- https://gitlab.com/dishantsethi/ekscf/-/blob/master/templates/eksiam.yml

## EKS network infrastructure

```A fully configured template for this is already available on the official AWS documentation website.```

- https://amazon-eks.s3-us-west-2.amazonaws.com/cloudformation/2019-10-08/amazon-eks-vpc-private-subnets.yaml

## EKS Cluster: Master Node

```Since we’re using a CloudFormation template though, we are going to let the template do all of the work for us in specifying the associated subnets, security group, and IAM role we want to use for the cluster creation.```

Append this to the main infrastructure template provided by amazon.
>
    EKSCluster:
    Type: AWS::EKS::Cluster
    Properties:
        Name: !Ref EKSClusterName
        RoleArn:
        "Fn::GetAtt": ["EKSIAMRole", "Arn"]
        ResourcesVpcConfig:
        SecurityGroupIds:
        - !Ref ControlPlaneSecurityGroup
        SubnetIds:
        - !Ref PublicSubnet01
        - !Ref PublicSubnet02
        - !Ref PrivateSubnet01
        - !Ref PrivateSubnet02
    DependsOn: [EKSIAMRole, PublicSubnet01, PublicSubnet02, PrivateSubnet01, PrivateSubnet02, ControlPlaneSecurityGroup]

### Final Template of Matser Node

- https://gitlab.com/dishantsethi/ekscf/-/blob/master/templates/sys.yml

## EKS Cluster: Worker nodes

```Creation of worker nodes is required to be a separate template because the creation of these nodes has the pre-requisite of the cluster actually existing first.```

- https://amazon-eks.s3-us-west-2.amazonaws.com/cloudformation/2019-10-08/amazon-eks-nodegroup.yaml

- https://gitlab.com/dishantsethi/ekscf/-/blob/master/templates/eksworkernode.yml


# EKS CF Template Test using Taskcat

Reference: 
- https://aws-quickstart.github.io/taskcat/
- https://github.com/aws-quickstart/quickstart-amazon-eks/tree/main/templates

Follow the guidelines to Setup the project : https://gitlab.com/dishantsethi/ekscf/-/blob/master/SETUP.md

## Verify : 
```
taskcat
```

## Run Test:
```
cd project-directory
taskcat test run
```

```Verify the CloudFormation template has launched by going to the CloudFormation dashboard. ```

---

```Once the stack is CREATE_COMPLETE, select it and click on the Outputs directory and open index.html to view taskcat dashboard.```