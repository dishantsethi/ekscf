# CF Testing with Taskcat

Minimum requirements: **pip, python3.7, virtualenv, awscli & taskcat**.

In your terminal, type or copy-paste the following:

    git clone git@gitlab.com:dishantsethi/ekscf.git; cd ekscf; 

Go grab a cup of coffee, we bake your hot development machine.
```
activate virtual environment
pip install -r requirements.txt
```

## Verify :
```
taskcat
```

## Run Test:
```
cd project-directory
taskcat test run
```

```Verify the CloudFormation template has launched by going to the CloudFormation dashboard. ```

---

```Once the stack is CREATE_COMPLETE, select it and click on the Outputs directory and open index.html to view taskcat dashboard.```